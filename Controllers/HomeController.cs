﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PGM_online.Models;

namespace PGM_online.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("MyFirstView");

        }

        public IActionResult SupervisorInfo()
        {
            List<Supervisor> supervisor = new List<Supervisor>()
            {
                new Supervisor{Name = "Tucker", Id = 2, isAvailable = false, Level = "New"},
                new Supervisor{Name = "Tim", Id = 2, isAvailable = false, Level = "Pro"},
                new Supervisor{Name = "David", Id = 2, isAvailable = false, Level = "Average"},
                new Supervisor{Name = "Solveig", Id = 2, isAvailable = false, Level = "Rookie"},
                new Supervisor{Name = "Liv", Id = 2, isAvailable = false, Level = "Elite"}
            };

            return View(supervisor);
        }
      

        public IActionResult timeStamp()
        {
           string dateTime = DateTime.Now.ToString();
           Time time = new Time { TimeStamp = DateTime.Now.ToString("h:mm:ss tt")};

            return View(dateTime);
        }

    }

}
